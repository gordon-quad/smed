#!/usr/bin/python3
from gi.repository import GLib
import dbus
import dbus.mainloop.glib
import sys
import datetime
import os
import subprocess

MM_BUS_NAME = "org.freedesktop.ModemManager1"

# fuck you XDG
hooks_path = os.path.join(os.getenv("HOME"), ".config/smed/hooks")

def exec_hooks(name, args, stdin=None):
    hooks = []
    if os.path.exists(os.path.join(hooks_path, name)):
        hooks = os.path.join(hooks_path, name)
    if os.path.isdir(os.path.join(hooks_path, name + '.d')):
        dirpath = os.path.join(hooks_path, name + '.d')
        hooks += sorted([f for f in map(lambda x: os.path.join(dirpath, x), os.listdir(dirpath)) if os.path.isfile(f)])
    for h in hooks:
        p = subprocess.Popen([h] + args, stdin=subprocess.PIPE, stdout=subprocess.DEVNULL, stdout=subprocess.DEVNULL)
        if stdin:
            p.stdin.write(stdin)
        p.stdin.close()
        p.wait()
        if p.returncode > 0:
            break

def modem_state_changed(old, new, reason):
    args = [old, new, reason]
    exec_hooks('modem/changed', args)

def messaging_added(path):
    obj = bus.get_object(MM_BUS_NAME, path)
    props = obj.GetAll('org.freedesktop.ModemManager1.Sms', dbus_interface='org.freedesktop.DBus.Properties')
    args = [path,
            props["State"],
            props["PduType"],
            props["Number"],
            props["SMSC"],
            props["Validity"],
            props["Class"],
            props["TeleserviceId"],
            props["ServiceCategory"],
            props["DeliverReportRequest"],
            props["MessageReference"],
            props["Timestamp"],
            props["DischargeTimestamp"],
            props["DeliveryState"],
            props["Storage"]]
    stdin = props["Text"]
    exec_hooks('sms/added', args, stdin)

def messaging_deleted(path):
    obj = bus.get_object(MM_BUS_NAME, path)
    props = obj.GetAll('org.freedesktop.ModemManager1.Sms', dbus_interface='org.freedesktop.DBus.Properties')
    args = [path,
            props["State"],
            props["PduType"],
            props["Number"],
            props["SMSC"],
            props["Validity"],
            props["Class"],
            props["TeleserviceId"],
            props["ServiceCategory"],
            props["DeliverReportRequest"],
            props["MessageReference"],
            props["Timestamp"],
            props["DischargeTimestamp"],
            props["DeliveryState"],
            props["Storage"]]
    stdin = props["Text"]
    exec_hooks('sms/deleted', args, stdin)

def time_network_time_changed(time):
    args = [time]
    exec_hooks('time/changed', args)

def oma_session_state_changed(old_session_state, new_session_state, session_state_failed_reason):
    args = [old_session_state, new_session_state, session_state_failed_reason]
    exec_hooks('oma/changed', args)

def cdma_activation_state_changed(activation_state, activation_error, status_changes):
    args = [activation_state, activation_error]
    exec_hooks('cdma/changed', args)

def voice_call_added(path):
    obj = bus.get_object(MM_BUS_NAME, path)
    props = obj.GetAll('org.freedesktop.ModemManager1.Call', dbus_interface='org.freedesktop.DBus.Properties')
    args = [path,
            props["State"],
            props["StateReason"],
            props["Direction"],
            props["Number"]]
    exec_hooks('call/added', args)

def voice_call_deleted(path):
    obj = bus.get_object(MM_BUS_NAME, path)
    props = obj.GetAll('org.freedesktop.ModemManager1.Call', dbus_interface='org.freedesktop.DBus.Properties')
    args = [path,
            props["State"],
            props["StateReason"],
            props["Direction"],
            props["Number"]]
    exec_hooks('call/deleted', args)

def call_state_changed(old, new, reason, path):
    obj = bus.get_object(MM_BUS_NAME, path)
    props = obj.GetAll('org.freedesktop.ModemManager1.Call', dbus_interface='org.freedesktop.DBus.Properties')
    args = [path,
            old,
            new,
            reason,
            props["Direction"],
            props["Number"]]
    exec_hooks('call/changed', args)

def call_dtmf_received(dtmf, path):
    obj = bus.get_object(MM_BUS_NAME, path)
    props = obj.GetAll('org.freedesktop.ModemManager1.Call', dbus_interface='org.freedesktop.DBus.Properties')
    args = [path,
            dtmf,
            props["State"],
            props["StateReason"],
            props["Direction"],
            props["Number"]]
    exec_hooks('call/dtmf', args)

if __name__ == '__main__':
    dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)

    bus = dbus.SystemBus()

    bus.add_signal_receiver(modem_state_changed,
            bus_name=MM_BUS_NAME,
            dbus_interface='org.freedesktop.ModemManager1.Modem',
            signal_name="StateChanged")
    bus.add_signal_receiver(messaging_added,
            bus_name=MM_BUS_NAME,
            dbus_interface='org.freedesktop.ModemManager1.Modem.Messaging',
            signal_name="Added")
    bus.add_signal_receiver(messaging_deleted,
            bus_name=MM_BUS_NAME,
            dbus_interface='org.freedesktop.ModemManager1.Modem.Messaging',
            signal_name="Deleted")

    bus.add_signal_receiver(time_network_time_changed,
            bus_name=MM_BUS_NAME,
            dbus_interface='org.freedesktop.ModemManager1.Modem.Time',
            signal_name="NetworkTimeChanged")

    bus.add_signal_receiver(oma_session_state_changed,
            bus_name=MM_BUS_NAME,
            dbus_interface='org.freedesktop.ModemManager1.Modem.Oma',
            signal_name="SessionStateChanged")

    bus.add_signal_receiver(cdma_activation_state_changed,
            bus_name=MM_BUS_NAME,
            dbus_interface='org.freedesktop.ModemManager1.Modem.ModemCdma',
            signal_name="ActivationStateChanged")

    bus.add_signal_receiver(voice_call_added,
            bus_name=MM_BUS_NAME,
            dbus_interface='org.freedesktop.ModemManager1.Modem.Voice',
            signal_name="CallAdded")
    bus.add_signal_receiver(voice_call_deleted,
            bus_name=MM_BUS_NAME,
            dbus_interface='org.freedesktop.ModemManager1.Modem.Voice',
            signal_name="CallDeleted")

    bus.add_signal_receiver(call_state_changed,
            bus_name=MM_BUS_NAME,
            dbus_interface='org.freedesktop.ModemManager1.Call',
            signal_name="StateChanged",
            path_keyword="path")
    bus.add_signal_receiver(call_dtmf_received,
            bus_name=MM_BUS_NAME,
            dbus_interface='org.freedesktop.ModemManager1.Call',
            signal_name="DtmfReceived",
            path_keyword="path")

    mainloop = GLib.MainLoop()
    mainloop.run()
